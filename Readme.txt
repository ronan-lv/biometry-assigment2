Configuration :
opencv 4.4.0
Python 3.8.2


In this project, I trained differents Viola-Jones cascade classifier 
in order to detect ears on pictures.
The methods and results are shown on the notebook final.ipynd