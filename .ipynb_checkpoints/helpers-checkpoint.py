import cv2
import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

def extract_box_dimensions(img, x, y):
    width, height = 0, 0
    while img[x][y + width] == 1:
        width += 1
        if y + width + 1 >= np.size(img, 1):
            break
        while img[x + height][y] == 1:
            height += 1
            if x + height + 1 >= np.size(img, 0):
                break
    return width, height


def extract_box_coordinates_out_of_the_mask(path_to_picture_mask):
    img_mask = cv2.imread(path_to_picture_mask)
    img_mask = cv2.cvtColor(img_mask, cv2.COLOR_BGR2GRAY)
    (thresh, img_mask) = cv2.threshold(img_mask, 127, 1, cv2.THRESH_BINARY)
    
    #Find rectangles in the image mask
    list_rectangles = []
    for x in range(0, np.size(img_mask, 0)):
        for y in range(0, np.size(img_mask, 1)):
            if img_mask[x][y] ==  1:
                if not list_rectangles:
                    width, height = extract_box_dimensions(img_mask, x, y)
                    tuple_box_data = (x, y, width, height)
                    list_rectangles.append(tuple_box_data)
                else:
                    is_cursor_in_already_saved_box = False
                    for rectangle in list_rectangles:
                        if  x >= rectangle[0] and x <= (rectangle[0] + rectangle[3]) and y >= rectangle[1] and (y <= rectangle[1] + rectangle[2]):
                            is_cursor_in_already_saved_box = True
                    if is_cursor_in_already_saved_box == False:
                        width, height = extract_box_dimensions(img_mask, x, y)
                        tuple_box_data = (x, y, width, height)
                        list_rectangles.append(tuple_box_data)
                        break
    list_return = []
    for rectangle in list_rectangles:
        list_return.append((rectangle[1], rectangle[0], rectangle[2], rectangle[3]))
        
    return list_return


def generate_vector_file_command(path_tool, path_positive_txt, path_positive_vec, width, height, num_vect):
    path_tool = path_tool.replace('/', '\\')
    path_positive_txt = path_positive_txt.replace('/', '\\')
    path_positive_vec = path_positive_vec.replace('/', '\\')
    
    return path_tool + ' -info ' + path_positive_txt + ' -w ' + str(width) + ' -h ' + str(height) + ' -num ' + str(num_vect) + ' -vec ' + path_positive_vec

def generate_train_model_command(path_tool, path_cascade_folder, path_vect_file, path_neg_file, width, height, numPositive, numNegative, numStages, minHitRate, maxFalseAlarmRate, ramDedicated):
    path_tool = path_tool.replace('/', '\\')
    path_cascade_folder = path_cascade_folder.replace('/', '\\')
    path_vect_file = path_vect_file.replace('/', '\\')
    path_neg_file = path_neg_file.replace('/', '\\')
    
    ram = ramDedicated/2
    
    cmd = path_tool + ' -data ' + path_cascade_folder + ' -vec ' + path_vect_file + ' -bg ' + path_neg_file + ' -w ' + str(width) + ' -h ' + str(height) 
    cmd += ' -numPos ' + str(numPositive) + ' -numNeg ' + str(numNegative) + ' -numStages ' + str(numStages) + ' -precalcValBufSize ' + str(ram) + ' -precalcIdxBufSize '+ str(ram) + ' -minHitRate ' + str(minHitRate) + ' -maxFalseAlarmRate ' + str(maxFalseAlarmRate)
    return cmd

def are_boxes_matching(rectangle_found, rectangle_true):
    #define by how  we size up the box
    offset =  5
    rectangle_found_offset = (rectangle_found[0] - offset, rectangle_found[1] - offset, rectangle_found[2] + offset*2, rectangle_found[3] + offset*2)
    rectangle_true_offset = (rectangle_true[0] - offset, rectangle_true[1] - offset, rectangle_true[2] + offset*2, rectangle_true[3] + offset*2)
    match = False;
    if rectangle_found[0] >=  rectangle_true_offset[0] and rectangle_found[0] <= rectangle_true_offset[0] + rectangle_true_offset[2] and rectangle_found[1] >= rectangle_true_offset[1] and rectangle_found[1] <= rectangle_true_offset[1] + rectangle_true_offset[3] and rectangle_found[0] + rectangle_found[2] >= rectangle_true_offset[0] and rectangle_found[0] + rectangle_found[2] <= rectangle_true_offset[0] + rectangle_true_offset[2] and rectangle_found[1] + rectangle_found[3] >= rectangle_true_offset[1] and rectangle_found[1] + rectangle_found[3] <= rectangle_true_offset[1] + rectangle_true_offset[3]:
        #print("The predicted rectangle is inside the ground truth rectangle")
        match = True
    elif rectangle_true[0] >=  rectangle_found_offset[0] and rectangle_true[0] <= rectangle_found_offset[0] + rectangle_found_offset[2] and rectangle_true[1] >= rectangle_found_offset[1] and rectangle_true[1] <= rectangle_found_offset[1] + rectangle_found_offset[3] and rectangle_true[0] + rectangle_true[2] >= rectangle_found_offset[0] and rectangle_true[0] + rectangle_true[2] <= rectangle_found_offset[0] + rectangle_found_offset[2] and rectangle_true[1] + rectangle_true[3] >= rectangle_found_offset[1] and rectangle_true[1] + rectangle_true[3] <= rectangle_found_offset[1] + rectangle_found_offset[3]:
        #print("The ground truth is inside the predicted rectangle")
        match = True
    
    return match

def evaluate_classifier(classifier, path_testing_set_pictures, path_testing_set_mask, size_training_set, scale_step, min_neighbords):
    
    correctly_predict = 0
    total_predict = 0
    total_true = 0
    
    list_test_pictures = os.listdir(path_testing_set_pictures)
    
    
    counter = 0
    for name_test_picture in list_test_pictures:
        img_test = cv2.imread(path_testing_set_pictures + name_test_picture)
        
        if scale_step == 0 or min_neighbords == 0:
            rectangles_results = classifier.detectMultiScale(img_test)
        else:
            rectangles_results = classifier.detectMultiScale(img_test, scale_step, min_neighbords)
            
        for rectangle in rectangles_results:
            total_predict+=1
        for rectangle_true in extract_box_coordinates_out_of_the_mask(path_testing_set_mask + name_test_picture):
            total_true+=1
            for rectangle_found in rectangles_results:
                if are_boxes_matching(rectangle_found, rectangle_true):
                    correctly_predict+=1 
                    break
         
        counter += 1
        if counter >= size_training_set:
            break
        
    return total_true, total_predict, correctly_predict


def plot_models_comparaison_diagramme(list_result_evaluation, title):
    dict_results_no_parameters = {}

    for classifier_name, classifier_results in list_result_evaluation.items():
    
        accuracy = round(classifier_results['correctly_predict']/classifier_results['total_predict'], 3)
        precision = round(classifier_results['correctly_predict']/classifier_results['total_true'], 3)
        dict_results_no_parameters[classifier_name] = {
            'total_true': classifier_results['total_true'],
            'total_predict': classifier_results['total_predict'],
            'correctly_predict': classifier_results['correctly_predict'],
            'accuracy': accuracy,
            'precision': precision
        }

    x = []
    y_precision = []
    y_accuracy = []
    for classifier_name, classifier_results in dict_results_no_parameters.items():
        x.append(classifier_name)
        y_accuracy.append(classifier_results['accuracy'])
        y_precision.append(classifier_results['precision'])

    #Final plot    
    plt.figure(figsize=(15,5))
    plt.plot(x, y_precision, 'ro')
    plt.plot(x, y_accuracy, 'bx')
    plt.xlabel('classifiers')
    plt.xticks(rotation=90)

    red_patch = mpatches.Patch(color='red', label='Accuracy')
    blue_patch = mpatches.Patch(color='blue', label='Precision')
    plt.title(title)
    plt.legend(handles=[red_patch, blue_patch])
    plt.show()